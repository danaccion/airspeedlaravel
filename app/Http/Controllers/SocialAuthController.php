<?php
    namespace App\Http\Controllers;
    use Illuminate\Support\Str;
    use Illuminate\Http\Request;
    use Socialite;
    class SocialAuthController extends Controller
    {
       private $apiToken;
       public function __construct()
        {
        $this->apiToken = uniqid(base64_encode(Str::random(40)));
        }
        public function redirect() {
             return Socialite::driver('facebook')->redirect();
        }
        public function callback() {
            $user = Socialite::with ( 'facebook' )->user ();
        
            return view ( 'home' )->withDetails ( $user )->withService ( 'facebook' );
        
        }
    }